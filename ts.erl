-module(ts).
-compile(export_all).

new() -> spawn_link(ts, tupleSpace, [[],[]]).

tupleSpace(Tuples, Queue) ->
    io:fwrite("Tuples now: ~p\nQueue now: ~p\n", [Tuples,Queue]),
    receive
        {post, X} -> 
            broadcastReturn(X, Queue),
            tupleSpace(Tuples ++ [X], Queue);
        {remove, Pid, Ref, X} -> 
			io:fwrite("Received ~p\n",[Ref]),
            case matchAll(X, Tuples) of
                true -> 
					io:fwrite("Okay, sending back to ~p\n",[Ref]),
                    Pid ! {getMatched(X, Tuples), Ref, ok},
                    tupleSpace(delete(getMatched(X, Tuples), Tuples), delete({Pid,Ref},Queue));  % Remove from tupleSpace and queue
                false -> 
                    io:fwrite("Blocked.\n"), % block request
					tupleSpace(Tuples, conditionalAdd(Queue, {Pid, Ref}))
            end;
		X -> io:fwrite("Received: ~p\n",[X])
    end.

conditionalAdd(List, Elem) -> 
	case contains(List, Elem) of
		false -> List ++ [Elem];
		true -> List
	end.


contains([], _) -> false;
contains([A|As], Elem) -> 
	if
		A == Elem -> true;
		true -> contains(As, Elem)
	end.

broadcastReturn(_, []) -> ok;
broadcastReturn(X,[{Pid,Ref}|Pids]) -> 
    Pid ! {X, Ref, ok},
    broadcastReturn(X,Pids).
    
    

getMatched(_, []) -> error;
getMatched(A, [X|XS]) ->
	case match(A, X) of
		true -> X;
		false -> getMatched(A,XS)
	end.


matchAll(_, []) -> false;
matchAll(Pattern, [Tuple|Tuples]) ->
    case match(Pattern, Tuple) of
        true -> true;
        false -> matchAll(Pattern, Tuples)
    end.
    
out(TS, Tuple) ->
    TS ! {post, Tuple}.
    

in(TS, Tuple) -> inHelp(TS, Tuple, make_ref()).

inHelp(TS, Tuple, Ref) ->
    TS ! {remove, self(), Ref, Tuple},
	io:fwrite("Sent request from ~p\n",[{remove, self(), Ref, Tuple}]),
	io:fwrite("Sent to: ~p\n",[TS]),
    receive
        {X, Ref, ok} -> 
		io:fwrite("Okay, I got ~p finally!\n", [X]),
		X
    end.
    
match(any,_) -> true;
match(P,Q) when is_tuple(P), is_tuple(Q) -> 
    match(tuple_to_list(P),tuple_to_list(Q));
match([P|PS],[L|LS]) -> 
    case match(P,L) of
        true -> match(PS,LS);
        false -> false
    end;
match(P,P) -> true;
match(_,_) -> false.

delete(Elem, List) ->
    deleteHelp(Elem, List, []).
    
deleteHelp(_, [], List) -> List;
deleteHelp(Elem, [X|XS], List) ->
    if
        X == Elem -> List ++ XS;
        true -> deleteHelp(Elem, XS, List ++ [X])
    end.
