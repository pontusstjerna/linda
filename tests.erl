-module(tests).
-compile(export_all).
-import(ts,[in/2,out/2,new/0]).
-import(random, [uniform/0]).

test() -> [doSomething(new()) || _ <- lists:seq(1,9000)].

test(N) -> 
	TS = new(),
	[doSomething(TS) || _ <- lists:seq(1,N)].

checkList([]) -> true;
checkList([X|XS]) -> X and checkList(XS).

doSomething(TS) -> 
	A = 10.0 * uniform(),
	io:fwrite("NR: ~p\n", [A]),
	if 
		A > 5 -> 
		out(TS, randomTuple(0));
		true -> 
		spawn(ts, in, [TS, randomTuple(any)])
	end.

randomTuple(Any) -> 
	A = 10.0 * uniform(),
	if
		A > 3 -> { 0, round(A)};
		A > 7-> { 1, round(A)};
		true -> { Any, round(A)}
	end.
