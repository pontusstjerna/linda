-module(examples).
-compile(export_all).
-import(linda,[in/2,out/2,new/0]).

example() ->
	TS = new(), %%Adds a new tuple space
	out(TS, {cheese}), %%Puts the tuple with cheese in the tuple space
	in(TS, {cheese}), %%Grabs the tuple with cheese. Now it doesn't block the thread because it is available.
	out(TS, {sausage, milk, "eggs"}), %%Puts a new tuple into the tuple space
	%%A shopper comes to the tuplespace and wants biological eggs among other things,
	%%but it is not available so it becomes blocked.
	Shopper = spawn_link(linda, in, [TS, {sausage, milk, "biological eggs"}]), 
	%%Now the eggs are available so the shopper buys them, because it is first in the queue
	out(TS, {sausage, milk, "biological eggs"}),
	%%Another shopper comes by and does not care at all what the last element in the tuple is
	in(TS, {sausage, milk, any}).
