-module(linda).
-compile(export_all).

new() -> spawn_link(linda, tupleSpace, [[],[]]).

tupleSpace(Tuples, Queue) ->
    io:fwrite("Tuples now: ~p\nQueue now: ~p\n", [Tuples,Queue]),
    receive
        {post, X} -> 
            broadcastReturn(Queue),
            tupleSpace(Tuples ++ [X], Queue);
        {remove, Pid, X} -> 
            case matchAll(X, Tuples) of
                true -> 
                    Pid ! {getMatched(X, Tuples), ok},
                    tupleSpace(delete(getMatched(X, Tuples), Tuples), delete(Pid, Queue));  % Remove from tupleSpace and queue
                false -> 
                    io:fwrite("Blocked.\n"), % block request
		tupleSpace(Tuples, conditionalAdd(Queue, Pid))
            end;
	{ntuples, Sender} ->
		Sender ! length(Tuples),
		tupleSpace(Tuples, Queue);
	{nqueue, Sender} ->
		Sender ! length(Queue),
		tupleSpace(Tuples, Queue)
    end.

nTuples(TS) ->
	TS ! {ntuples, self()},
	receive
		X -> X
	end.

nQueue(TS) ->
	TS ! {nqueue, self()},
	receive
		X -> X
	end.

conditionalAdd(List, Elem) -> 
	case contains(List, Elem) of
		false -> List ++ [Elem];
		true -> List
	end.


contains([], _) -> false;
contains([A|As], Elem) -> 
	if
		A == Elem -> true;
		true -> contains(As, Elem)
	end.

broadcastReturn([]) -> ok;
broadcastReturn([Pid|Pids]) -> 
    Pid ! retry,
    broadcastReturn(Pids).
    
    

getMatched(_, []) -> error;
getMatched(A, [X|XS]) ->
	case match(A, X) of
		true -> X;
		false -> getMatched(A,XS)
	end.


matchAll(_, []) -> false;
matchAll(Pattern, [Tuple|Tuples]) ->
    case match(Pattern, Tuple) of
        true -> true;
        false -> matchAll(Pattern, Tuples)
    end.
    
out(TS, Tuple) ->
    TS ! {post, Tuple}.
    
in(TS, Tuple) ->
    TS ! {remove, self(), Tuple},
    receive
        {X, ok} -> 
		io:fwrite("Okay, I got ~p finally!\n", [X]),
		X;
        retry -> 
            io:fwrite("Someone turned in, trying again!\n"),
            in(TS, Tuple)
    end.
    
match(any,_) -> true;
match(P,Q) when is_tuple(P), is_tuple(Q) -> 
    match(tuple_to_list(P),tuple_to_list(Q));
match([P|PS],[L|LS]) -> 
    case match(P,L) of
        true -> match(PS,LS);
        false -> false
    end;
match(P,P) -> true;
match(_,_) -> false.

delete(Elem, List) ->
    deleteHelp(Elem, List, []).
    
deleteHelp(_, [], List) -> List;
deleteHelp(Elem, [X|XS], List) ->
    if
        X == Elem -> List ++ XS;
        true -> deleteHelp(Elem, XS, List ++ [X])
    end.
